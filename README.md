**Task N1

[In There](https://reqres.in/) has test API requests. We need to show users listing with pagination, and the details on required parameters are written in the site.

1. In the listing we need to show the user emails with their avatars
2. It should be possible to go from the listing to a single user profile, where the whole information will be presented
3. The first time the user opens the application, the data should load from API request and should be stored in the local database (the database option is up to you). Next time when the user opens the listing, the locally stored users should be shown first, then an API request should happen, following with an update on the listing and local database.
4. The same should happen to the single user page.
5. OOP principles should be strictly followed throughout the project implementation.