//
//  ServiceAssembly.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import RxRestClient
import Alamofire


/// Inject services to container
final class ServiceAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(RealmServiceProtocol.self, initializer: RealmService.init).inObjectScope(.container)
        container.autoregister(PreferencesServiceProtocol.self, initializer: PreferencesService.init)
        container.autoregister(UsersServiceProtocol.self, initializer: UsersService.init)

        container.autoregister(SessionManager.self, initializer: provideSessionManager)
        container.autoregister(RxRestClient.self, initializer: provideRestClient)
    }

    private func provideRestClient(_ manager: SessionManager) -> RxRestClient {
        var options = RxRestClientOptions.default

        options.logger = DebugRxRestClientLogger()
        options.jsonDecoder = JSONDecoder()
        options.jsonEncoder = JSONEncoder()
        options.sessionManager = manager
        options.urlEncoding = URLEncoding(boolEncoding: .literal)

        return RxRestClient(baseUrl: URL(string: Config.BASE_URL), options: options)
    }

    private func provideSessionManager(_ adapter: PreferencesServiceProtocol) -> SessionManager {
        let manager = SessionManager.default

        manager.adapter = adapter

        return manager
    }
}
