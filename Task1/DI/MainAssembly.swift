//
//  MainAssembly.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import RxRestClient
import Alamofire

final class MainAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(UsersListViewModel.self, initializer: UsersListViewModel.init)
        container.register(UsersListViewController.self) { r in
            let controller = UsersListViewController.instantiate()
            controller.viewModel = r ~> UsersListViewModel.self
            return controller
        }

        container.autoregister(UserDetailsViewModel.self, initializer: UserDetailsViewModel.init)
        container.register(UserDetailsViewController.self) { r in
            let controller = UserDetailsViewController.instantiate()
            controller.viewModel = r ~> UserDetailsViewModel.self
            return controller
        }
    }
}
