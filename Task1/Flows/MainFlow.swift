//
//  MainFlow.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxFlow
import Swinject
import SwinjectAutoregistration

final class MainFlow: HasFlow {

    var rootViewController: UIViewController = UINavigationController()

    var parentAssembler: Assembler?

    var assembler: Assembler

    var assemblies: [Assembly] = [
        MainAssembly()
    ]

    init(parentAssembler: Assembler? = nil) {
        self.parentAssembler = parentAssembler
        self.assembler = Assembler(assemblies, parent: parentAssembler)
    }

    func navigate(to step: AppStep) -> FlowContributors {
        switch step {
        case .list:
            return push(UsersListViewController.self)
        case .details(let context):
            return push(UserDetailsViewController.self) { $0.context = context }
        }
    }
}
