//
//  AppFlow.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxFlow
import Swinject
import SwinjectAutoregistration

final class AppFlow: Flow {

    var root: Presentable {
        return rootWindow
    }

    private let rootWindow: UIWindow
    private let parentAssembler: Assembler?

    init(withWindow window: UIWindow, parentAssembler: Assembler? = nil) {
        rootWindow = window
        self.parentAssembler = parentAssembler
    }

    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppStep else { return .none }
        switch step {
        case .list:
            return navigationToHome()
        default:
            return .none
        }
    }

    private func navigationToHome() -> FlowContributors {
        let mainFlow = MainFlow(parentAssembler: parentAssembler)
        Flows.whenReady(flow1: mainFlow) { [unowned self] root in
            self.rootWindow.rootViewController = root
        }
        return .one(
            flowContributor: .contribute(
                withNextPresentable: mainFlow,
                withNextStepper: OneStepper(withSingleStep: AppStep.list)
            )
        )
    }

}
