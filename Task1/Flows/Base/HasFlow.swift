//
//  HasFlow.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import RxFlow
import Swinject
import SwinjectAutoregistration

/// Protocol with base navigation system
protocol HasFlow: Flow {

    /// Root of Flow
    var rootViewController: UIViewController { get set }

    /// Parent Assabler
    var parentAssembler: Assembler? { get set }

    /// Current Assambler
    var assembler: Assembler { get set }

    /// Assemblies for current fLow
    var assemblies: [Assembly] { get }

    /// Navigate to given step
    ///
    /// - Parameter step: AppStep for detecting direction of navigating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func navigate(to step: AppStep) -> FlowContributors

    /// Push to ViewController
    ///
    /// - Parameters:
    ///   - viewController: Direction ViewController
    ///   - animated: Push with animation default` true
    ///   - configurator: Closure with prepared ViewController for inner parameters manipulating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func push<T: BaseViewController>(
        _ viewController: T.Type,
        animated: Bool,
        configurator: @escaping ((T) -> Void)) -> FlowContributors

    /// Present ViewController
    ///
    /// - Parameters:
    ///   - viewController: Direction ViewController
    ///   - animated: Present with animation default` true
    ///   - configurator: Closure with prepared ViewController for inner parameters manipulating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func present<T: BaseViewController>(
        _ viewController: T.Type,
        animated: Bool,
        configurator: @escaping ((T) -> Void)) -> FlowContributors

    /// Replace last with ViewController
    ///
    /// - Parameters:
    ///   - viewController: Direction ViewController
    ///   - animated: Replace with animation default` true
    ///   - configurator: Closure with prepared ViewController for inner parameters manipulating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func replaceLast<T: BaseViewController>(
        with viewController: T.Type,
        animated: Bool,
        configurator: @escaping ((T) -> Void)) -> FlowContributors

}

extension HasFlow {
    var root: Presentable {
        return rootViewController
    }

    /// Navigate to given step
    ///
    /// - Parameter step: AppStep for detecting direction of navigating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func navigate(to step: Step) -> FlowContributors {
        guard let step = step as? AppStep else { return .none }
        return navigate(to: step)
    }

    /// Push to ViewController
    ///
    /// - Parameters:
    ///   - viewController: Direction ViewController
    ///   - animated: Push with animation default` true
    ///   - configurator: Closure with prepared ViewController for inner parameters manipulating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func push<T: BaseViewController>(
        _ viewController: T.Type,
        animated: Bool = true,
        configurator: @escaping ((T) -> Void) = {_ in}) -> FlowContributors {
        let viewController = assembler.resolver ~> viewController.self
        configurator(viewController)
        guard let presenter = rootViewController.navigationController ?? (rootViewController as? UINavigationController) else {
            fatalError("Can't find Presenter's Navigation controller")
        }
        presenter.pushViewController(viewController, animated: animated)
        return .one(flowContributor: .contribute(withNextPresentable: viewController, withNextStepper: viewController.stepper))
    }

    /// Present ViewController
    ///
    /// - Parameters:
    ///   - viewController: Direction ViewController
    ///   - animated: Present with animation default` true
    ///   - configurator: Closure with prepared ViewController for inner parameters manipulating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func present<T: BaseViewController>(
        _ viewController: T.Type,
        animated: Bool = true,
        configurator: @escaping ((T) -> Void) = {_ in}) -> FlowContributors {
        let viewController = assembler.resolver ~> viewController.self
        configurator(viewController)
        rootViewController.present(viewController, animated: animated, completion: nil)
        return .one(flowContributor: .contribute(withNextPresentable: viewController, withNextStepper: viewController.stepper))
    }

    /// Replace last with ViewController
    ///
    /// - Parameters:
    ///   - viewController: Direction ViewController
    ///   - animated: Replace with animation default` true
    ///   - configurator: Closure with prepared ViewController for inner parameters manipulating
    /// - Returns: FlowContributors for setup stepper in navigated view
    func replaceLast<T: BaseViewController>(
        with viewController: T.Type,
        animated: Bool = true,
        configurator: @escaping ((T) -> Void) = {_ in}) -> FlowContributors {
        let viewController = assembler.resolver ~> viewController.self
        configurator(viewController)
        guard let nvc = (self.rootViewController as? UINavigationController) else {
            return .none
        }
        var vcs = nvc.viewControllers
        vcs.removeLast()
        vcs.append(viewController)
        nvc.setViewControllers(vcs, animated: animated)
        return .one(flowContributor: .contribute(withNextPresentable: viewController, withNextStepper: viewController.stepper))
    }

}
