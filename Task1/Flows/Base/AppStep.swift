//
//  AppStep.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import RxFlow

/// All app steps
///
/// - list: UsersListViewController
/// - details: UserDetailsViewController
enum AppStep: Step {
    case list
    case details(context: UserDetailsContext)
}
