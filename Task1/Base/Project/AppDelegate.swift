//
//  AppDelegate.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/30/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator
import RxFlow
import Swinject
import SwinjectAutoregistration

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var coordinator = FlowCoordinator()
    private var appFlow: AppFlow!
    private var parentAssembler = Assembler([ServiceAssembly()])

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        guard let window = self.window else { return false }
        self.appFlow = AppFlow(withWindow: window, parentAssembler: parentAssembler)
        coordinator.coordinate(flow: appFlow, with: OneStepper(withSingleStep: AppStep.list))

        NetworkActivityIndicatorManager.shared.isEnabled = true
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}
