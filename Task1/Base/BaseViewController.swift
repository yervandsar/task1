//
//  BaseViewController.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa
import RxOptional
import RxFlow

/// Empty Stepper
final class NoneStepper: Stepper {
    var steps: PublishRelay<Step> {
        return PublishRelay<Step>()
    }

    init() { }
}

/// Storyboard based view controller with Stepper
class BaseViewController: UIViewController, StoryboardBased {
    private let disposeBag = DisposeBag()

    // MARK: - Stepper
    /// View Controllers Stepper for Flow Manipulations
    var stepper: Stepper {
        return NoneStepper()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
    }

    func initNavigationBar() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }

    deinit {
        print("-<|DEINIT: ---\(String(describing: self))---|>-")
    }

}
