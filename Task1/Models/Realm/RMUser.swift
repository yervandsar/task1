//
//  RMUser.swift
//  Task1
//
//  Created by Yervand Saribekyan on 9/1/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import RxSwift
import RxCocoa
import RxRealm
import RealmSwift

class RMUser: Object {

    @objc dynamic var id = Int(0)
    @objc dynamic var firstName: String?
    @objc dynamic var lastName: String?
    @objc dynamic var email = ""
    @objc dynamic var avatar: String?

    override static func primaryKey() -> String? {
        return "id"
    }

}

extension RMUser: UserInfoProtocol {
    var imageUrl: URL? {
        guard let strUrl = avatar else { return nil }
        return URL(string: strUrl)
    }
}
