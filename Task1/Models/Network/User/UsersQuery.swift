//
//  UsersQuery.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift
import RxRestClient

struct UsersQuery: PagingQueryProtocol {

    var page: Int

    init(initialPage: Int = 1) {
        self.page = initialPage
    }

    func nextPage() -> UsersQuery {
        var new = self
        new.page += 1
        return new
    }
}
