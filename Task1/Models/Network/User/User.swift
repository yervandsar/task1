//
//  User.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation

protocol UserInfoProtocol {
    var id: Int { get }
    var email: String { get }
    var firstName: String? { get }
    var lastName: String? { get }
    var imageUrl: URL? { get }
}

struct User: Decodable {
    var id: Int
    var email: String
    var firstName: String?
    var lastName: String?
    var avatar: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case email = "email"
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar = "avatar"
    }

    var rmObject: RMUser {
        let object = RMUser()
        object.id = id
        object.firstName = firstName
        object.lastName = lastName
        object.email = email
        object.avatar = avatar
        return object
    }
}

extension User: UserInfoProtocol {
    var imageUrl: URL? {
        guard let strUrl = avatar else { return nil }
        return URL(string: strUrl)
    }
}
