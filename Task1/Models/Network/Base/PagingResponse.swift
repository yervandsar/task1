//
//  PagingResponse.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxRestClient

struct PagingResponse<T: Decodable>: PagingResponseProtocol {
    var currentPage: Int
    var pageLimit: Int
    var itemsCount: Int
    var pagesCount: Int
    var result: [T] = []

    enum CodingKeys: String, CodingKey {
        case currentPage = "page"
        case pageLimit = "per_page"
        case itemsCount = "total"
        case pagesCount = "total_pages"
        case result = "data"
    }

    // MARK: - PagingResponseProtocol
    typealias Item = T

    static var decoder: JSONDecoder {
        return .init()
    }

    var canLoadMore: Bool {
        return currentPage < pagesCount
    }

    var items: [T] {
        get {
            return result
        }
        set(newValue) {
            result = newValue
        }
    }
}
