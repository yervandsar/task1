//
//  SingleState.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxRestClient

class SingleState<T: Decodable>: ResponseState {
    typealias Body = Data

    var state: BaseState?
    var success: Bool = false
    var response: T?

    required init(state: BaseState) {
        self.state = state
        success = false
    }

    required init(response: (HTTPURLResponse, Data?)) {
        state = BaseState.online
        success = (200 ..< 300).contains(response.0.statusCode)
        guard let data = response.1 else { return }
        if success {
            do {
                self.response = try JSONDecoder().decode(T.self, from: data)
            } catch let error {
                self.state = BaseState(unexpectedError: error)
            }
        }
    }
}
