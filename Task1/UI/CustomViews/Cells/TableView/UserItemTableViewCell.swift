//
//  UserItemTableViewCell.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import Reusable
import SDWebImage

final class UserItemTableViewCell: UITableViewCell, NibReusable {
    static let identifier = "UserItemTableViewCell"

    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            userImageView.layer.cornerRadius = 30
        }
    }
    @IBOutlet weak var emailLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func render(user: UserInfoProtocol) {
        userImageView.setProfile(user.imageUrl, for: user.firstName)
        emailLabel.text = user.email
    }

}
