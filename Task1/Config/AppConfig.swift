//
//  AppConfig.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation

/// Enum of app's environments
enum AppConfig: String {
    case dev
    case alpha
    case live

    var DEBUG: Bool {
        switch self {
        case .live:
            return false
        default:
            return true
        }
    }

    var SECURE: Bool {
        return true
    }

    var HOST_NAME: String {
        switch self {
        case .dev:
            return "reqres.in"
        case .alpha:
            return ""
        case .live:
            return ""
        }
    }

    var BASE_URL: String {
        return "http\(self.SECURE ? "s" : "")://\(HOST_NAME)/api/"
    }
}
