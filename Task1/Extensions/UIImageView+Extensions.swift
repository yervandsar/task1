//
//  UIImageView+Extensions.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {

    /// Set Image from url with SDWebImage, if image is nil set first letter of name as placeholder image
    ///
    /// - Parameters:
    ///   - url: image url
    ///   - user: user name
    func setProfile(_ url: URL?, for user: String?) {
        let imageSnap = self.imageSnap(
            text: user?.first?.uppercased(),
            color: .black,
            circular: true,
            stroke: true,
            textAttributes: [
                NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 30)
            ]
        )
        self.sd_setImage(with: url, placeholderImage: imageSnap)
    }
}
