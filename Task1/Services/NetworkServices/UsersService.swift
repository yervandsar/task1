//
//  UsersService.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift
import RxRestClient

protocol UsersServiceProtocol {
    func getUsers(query: UsersQuery, loadNextPageTrigger: Observable<Void>) -> Observable<UsersState>
}

final class UsersService: BaseService, UsersServiceProtocol {
    func getUsers(query: UsersQuery, loadNextPageTrigger: Observable<Void>) -> Observable<UsersState> {
        return client.get("users", query: query, loadNextPageTrigger: loadNextPageTrigger)

    }
}
