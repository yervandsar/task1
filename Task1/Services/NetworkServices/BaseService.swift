//
//  BaseService.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Alamofire
import RxRestClient

class BaseService {
    let client: RxRestClient

    init(client: RxRestClient) {
        self.client = client
    }
}
