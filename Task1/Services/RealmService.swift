//
//  RealmService.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import RxSwift
import RxCocoa
import RealmSwift
import RxRealm

protocol RealmServiceProtocol {
    var realmScheduler: SerialDispatchQueueScheduler { get }
    var schemaVersion: UInt64 { get }
    var realm: Realm { get }

    var usersDataTrigger: Observable<[RMUser]> { get }
    func user(by id: Int) -> RMUser?
    func save(user: RMUser)
    func save(users: [RMUser])
}

final class RealmService: RealmServiceProtocol {
    var realmScheduler: SerialDispatchQueueScheduler {
        return MainScheduler.instance
    }

    var schemaVersion: UInt64 {
        return 1
    }

    var realm: Realm {
        return _realm
    }

    private var _realm: Realm = {
        do {
            return try Realm()
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }()

    // MARK: - Init
    init() {
        let config = Realm.Configuration(
            schemaVersion: schemaVersion,
            migrationBlock: migrationBlock(),
            shouldCompactOnLaunch: shouldCompactOnLaunch()
        )
        print("------------------------------REALM_FILE_URL-------------------------------")
        print(config.fileURL!)
        print("---------------------------------------------------------------------------")
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }

    var usersDataTrigger: Observable<[RMUser]> {
        let userData = realm.objects(RMUser.self)
        return Observable.array(from: userData).subscribeOn(realmScheduler)
    }

    func user(by id: Int) -> RMUser? {
        return realm.object(ofType: RMUser.self, forPrimaryKey: id)
    }

    func save(user: RMUser) {
        self.save(object: user)
    }

    func save(users: [RMUser]) {
        self.save(objects: users)
    }

}

fileprivate extension RealmService {
    func migrationBlock() -> MigrationBlock {
        return { _, _  in () }
    }

    func shouldCompactOnLaunch() -> ((Int, Int) -> Bool)? {
        return { totalBytes, usedBytes in
            let oneHundredMB = 100 * 1024 * 1024
            return (totalBytes > oneHundredMB) && (Double(usedBytes) / Double(totalBytes)) < 0.5
        }
    }

    func save(object: Object) {
        try? realm.write { [weak self] in
            self?.realm.add(object, update: .modified)
        }
    }

    func save(objects: [Object]) {
        try? realm.write { [weak self] in
            self?.realm.add(objects, update: .modified)
        }
    }
}
