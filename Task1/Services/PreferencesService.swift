//
//  PreferencesService.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import Alamofire

protocol PreferencesServiceProtocol: RequestAdapter {
    var lastSyncedPage: Int { get }
    func set(lastSyncedPage: Int)
}

enum PreferencesKeys: String {
    case lastPage
}

final class PreferencesService: PreferencesServiceProtocol {

    var lastSyncedPage: Int {
        return UserDefaults.standard.integer(forKey: PreferencesKeys.lastPage.rawValue)
    }

    func set(lastSyncedPage: Int) {
        UserDefaults.standard.set(lastSyncedPage, forKey: PreferencesKeys.lastPage.rawValue)
        UserDefaults.standard.synchronize()
    }

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        guard urlRequest.url?.absoluteString.contains(Config.BASE_URL) ?? false else {
            return urlRequest
        }
        // Here can be auth header manipulation logic
        return urlRequest
    }
}
