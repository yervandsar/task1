//
//  UsersListViewController.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxFlow
import RxCocoa

final class UsersListViewController: BaseViewController {

    override var stepper: Stepper {
        return viewModel
    }

    var viewModel: UsersListViewModel!

    @IBOutlet weak var tableView: UITableView!

    private let usersData = BehaviorRelay<[UserInfoProtocol]>(value: [])

    override func viewDidLoad() {
        super.viewDidLoad()
        doBindings()
        initTableView()
        viewModel.requestTrigger.onNext(())
    }

    override func initNavigationBar() {
        super.initNavigationBar()
        navigationItem.title = "Users List"
    }

    private func doBindings() {
        viewModel.users
            .bind(to: usersData)
            .disposed(by: rx.disposeBag)
    }

    private func initTableView() {
        tableView.register(cellType: UserItemTableViewCell.self)
        tableView.estimatedRowHeight = 80
        usersData
            .bind(to: tableView.rx.items) { (tableView, _, user) in
                let reusableCell = tableView.dequeueReusableCell(withIdentifier: UserItemTableViewCell.identifier)
                guard let cell = reusableCell as? UserItemTableViewCell else {
                        return UITableViewCell()
                }
                cell.render(user: user)
                return cell
            }
            .disposed(by: rx.disposeBag)

        tableView
            .rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let self = self else { return }
                self.tableView.deselectRow(at: indexPath, animated: false)
            })
            .disposed(by: rx.disposeBag)

        tableView
            .rx.modelSelected(UserInfoProtocol.self)
            .map { $0.id }
            .bind(to: viewModel.openDetails)
            .disposed(by: rx.disposeBag)

        tableView
            .rx.willDisplayCell
            .map { $0.indexPath }
            .withLatestFrom(usersData) { $0.row + 1 == $1.count }
            .filter { $0 }
            .map { _ in () }
            .bind(to: viewModel.loadMore)
            .disposed(by: rx.disposeBag)
    }

}
