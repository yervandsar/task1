//
//  UsersListViewModel.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFlow
import RxRestClient

final class UsersListViewModel: Stepper, HasDisposeBag {
    let steps = PublishRelay<Step>()

    let openDetails = PublishRelay<Int>()
    let requestTrigger = PublishSubject<Void>()
    let loadMore = PublishRelay<Void>()

    let users = BehaviorRelay<[UserInfoProtocol]>(value: [])
    let baseState = PublishRelay<BaseState>()

    private let realm: RealmServiceProtocol
    private let preferences: PreferencesServiceProtocol
    private let service: UsersServiceProtocol

    init(
        realm: RealmServiceProtocol,
        preferences: PreferencesServiceProtocol,
        service: UsersServiceProtocol) {
        self.realm = realm
        self.preferences = preferences
        self.service = service
        doBindings()
    }

    private func doBindings() {
        let state = requestTrigger
            .map { [preferences] _ in UsersQuery(initialPage: preferences.lastSyncedPage).nextPage() }
            .flatMapLatest { [service, loadMore] query in
                service.getUsers(query: query, loadNextPageTrigger: loadMore.asObservable())
            }
            .share()

        state
            .filter { ($0.response?.result ?? []).isNotEmpty }
            .map { $0.response?.currentPage }
            .filterNil()
            .subscribe(onNext: { [preferences] lastPage in
                preferences.set(lastSyncedPage: lastPage)
            })
            .disposed(by: disposeBag)

        state
            .map { $0.response?.result ?? [] }
            .filterEmpty()
            .map { $0.map { $0.rmObject } }
            .observeOn(realm.realmScheduler)
            .subscribe(onNext: { [realm] users in
                realm.save(users: users)
            })
            .disposed(by: disposeBag)

        realm
            .usersDataTrigger
            .map { $0.map { $0 as UserInfoProtocol } }
            .bind(to: users)
            .disposed(by: disposeBag)

        openDetails
            .map(UserDetailsContext.init)
            .map(AppStep.details)
            .bind(to: steps)
            .disposed(by: disposeBag)
    }
}
