//
//  UserDetailsViewModel.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFlow

final class UserDetailsViewModel: Stepper, HasDisposeBag {
    let steps = PublishRelay<Step>()

    let getDetails = PublishRelay<Int>()

    let usersData = PublishSubject<UserInfoProtocol>()

    private let realm: RealmServiceProtocol

    init(realm: RealmServiceProtocol) {
        self.realm = realm
        doBindings()
    }

    private func doBindings() {
        getDetails
            .map { [realm] id in realm.user(by: id) }
            .filterNil()
            .map { $0 as UserInfoProtocol }
            .bind(to: usersData)
            .disposed(by: disposeBag)
    }
}
