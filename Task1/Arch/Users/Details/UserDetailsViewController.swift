//
//  UserDetailsViewController.swift
//  Task1
//
//  Created by Yervand Saribekyan on 8/31/19.
//  Copyright © 2019 Yervand Saribekyan. All rights reserved.
//

import UIKit
import RxSwift
import RxFlow
import RxCocoa

final class UserDetailsViewController: BaseViewController {

    override var stepper: Stepper {
        return viewModel
    }

    var viewModel: UserDetailsViewModel!
    var context: UserDetailsContext!

    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            userImageView.layer.cornerRadius = 60
        }
    }
    @IBOutlet weak var fNameStack: UIStackView!
    @IBOutlet weak var fNameLabel: UILabel!
    @IBOutlet weak var lNameStack: UIStackView!
    @IBOutlet weak var lNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        doBindings()
        viewModel.getDetails.accept(context.id)
    }

    override func initNavigationBar() {
        super.initNavigationBar()
        navigationItem.title = "User Details"
    }

    private func doBindings() {
        viewModel
            .usersData
            .subscribe(onNext: { [weak self] user in
                self?.set(user: user)
            })
            .disposed(by: rx.disposeBag)
    }

    private func set(user: UserInfoProtocol) {
        fNameStack.isHidden = user.firstName == nil
        lNameStack.isHidden = user.lastName == nil
        fNameLabel.text = user.firstName
        lNameLabel.text = user.lastName
        emailLabel.text = user.email
        userImageView.setProfile(user.imageUrl, for: user.firstName)

    }
}
